module Read
  ( testReadVerbose,
    testRead,
    testReadHigh,
    testReadSingleChar,
  )
where

import Control.Concurrent.URing
import Control.Monad
  ( forM_,
    void,
  )
import Control.Monad.IO.Unlift
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import Foreign.C.String (peekCString)
import Foreign.C.Types (CChar)
import Foreign.Ptr (Ptr)
import System.Posix.IO
import System.Posix.Types (Fd)

data MyData = MyData {job :: Int, name :: String} deriving (Show)

testReadVerbose :: URing -> IO ()
testReadVerbose uring = withURingQueue 4 uring [] $ \_ -> do
  fd <- openFd "example/test.txt" ReadOnly Nothing defaultFileFlags
  sqe <- getSqe uring

  let buf = BS.replicate 20 0
  BS8.useAsCString buf $ \buf' -> do
    prepRead sqe fd buf' 20 0
    sqeSetData sqe MyData {job = 1, name = "read"}
    submitted <- submit uring
    print $ "submitted " <> show submitted <> " jobs"
    withCqe $ \cqe -> do
      waitCqe uring cqe
      res <- cqeResult cqe
      print $ "res was: " <> show res
      cqeData' <- cqeGetData cqe
      putStrLn $ "name was " <> show (fmap job cqeData')
      cqeSeen uring cqe
    y <- peekCString buf'
    putStrLn $ "read: " <> show y <> " from fd: " <> show fd
  closeFd fd

testRead :: URing -> IO ()
testRead uring = withURingQueue 4 uring [] $ \_ -> do
  fd <- openFd "example/test.txt" ReadOnly Nothing defaultFileFlags

  let buf = BS.replicate 20 0
  let myData = MyData {job = 1, name = "example/test.txt"}
  BS.useAsCStringLen buf $ \(buf', len) -> do
    sqe' <- uring `withSqeData` myData $ \sqe -> prepRead sqe fd buf' len 0
    void $ submit uring
    withCqe $ \cqe -> do
      waitCqe uring cqe
      myData' <- cqeGetData cqe
      cqeSeen uring cqe
      print (myData' :: Maybe MyData)

  closeFd fd

testReadHigh :: URing -> IO ()
testReadHigh uring = withQueue uring 4 $ \_ -> do
  fd <- liftIO $ openFd "example/test.txt" ReadOnly Nothing defaultFileFlags
  withRunInIO $ \runInIO ->
    BS.useAsCStringLen (BS.replicate 20 0) $ \(buf', len) -> runInIO $ do
      submission <- uRead buf' len fd 0
      setUserData submission buf'
      submitted <- submitRing
      completed <- complete submitted
      forM_ completed $ \completion -> do
        readBuf <- traverse (liftIO . peekCString) $ userData completion
        liftIO . print $ "read " <> show readBuf
  liftIO $ closeFd fd

testReadSingleChar :: URing -> IO ()
testReadSingleChar uring = withQueue uring 32 $ \_ -> do
  fd <- liftIO $ openFd "example/test.txt" ReadOnly Nothing defaultFileFlags
  submission <- mkRings fd 11 0 []
  submitted <- submitRing
  completed <- complete submitted
  forM_ completed $ \complete -> do
    buf <- traverse (liftIO . peekCString) $ userData complete
    liftIO $ print buf
  liftIO $ closeFd fd
  where
    mkRings ::
      Fd ->
      Int ->
      Int ->
      [RingSubmission (Ptr CChar)] ->
      RingM [RingSubmission (Ptr CChar)]
    mkRings _ 0 _ acc = pure $ reverse acc
    mkRings fd bytes offset acc = withRunInIO $ \runInIO ->
      BS.useAsCStringLen (BS.replicate 1 0) $ \(buf', len) -> runInIO $ do
        submission <- uRead buf' len fd offset
        submission' <- setUserData submission buf'
        mkRings fd (bytes - 1) (offset + 1) (submission' : acc)
