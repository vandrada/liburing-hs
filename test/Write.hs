module Write
  ( writeTests,
  )
where

import Common (getResultData)
import Control.Concurrent.URing
import Control.Monad
  ( forM,
    forM_,
    replicateM_,
    void,
  )
import qualified Data.ByteString.Char8 as BS8
import Data.List (intercalate)
import System.Posix.IO
import System.Posix.Types (Fd)
import Test.Hspec

type Meta = (Int, BS8.ByteString)

newtype Submission = Submission {submissionId :: Int} deriving (Show)

writeTests :: Spec
writeTests = describe "prepWrite" $ do
  testWrite

--testWriteHigh
--testLinkedWrite

toBuf :: Submission -> BS8.ByteString
toBuf = BS8.pack . show . submissionId

--testLinkedWrite :: Spec
--testLinkedWrite uring = do
--  let count = 10
--  fd <- openFd "example/test_out_sync.txt" WriteOnly (Just 644) defaultFileFlags
--
--  sqeWithSubmission <- forM [1 .. count] $ \n -> do
--    sqe <- getSqe uring
--    pure (sqe, Submission n)
--
--  -- starts a chain of asynchronous, but chained writes: writing "2" will only
--  -- be done after writing "1" has completed and writing "3" only after writing
--  -- "2", etc.
--  forM_ sqeWithSubmission $ \(sqe, submission) -> do
--    let toWrite = toBuf submission <> "\n"
--    BS8.useAsCStringLen toWrite $ \(buf', len) ->
--      prepWrite sqe fd buf' len (10 * submissionId submission)
--    sqeSetData sqe submission
--    sqeSetFlags sqe [Link]
--
--  ready <- sqReady uring
--  putStrLn $ "have: " <> show ready <> " submissions ready"
--
--  -- tail of the link since it has no `Link` flag: sync the file
--  uring `withSqe` \sqe -> do
--    prepFsync sqe fd []
--    sqeSetData sqe (Submission 0)
--
--  void $ submit uring
--
--  ready' <- cqReady uring
--  putStrLn $ "have: " <> show ready' <> " completions ready"
--
--  replicateM_ (count + 1) $ do
--    completed <- withCqeData $ cqeWaitAndSee uring
--    print (completed :: Submission)
--
testWrite :: Spec
testWrite = it "writes to a fd" $
  example $ do
    withURing $ \uring -> withURingQueue 10 uring [] $ \_ -> do
      let count = 10
      fd <-
        openFd
          writeFileName
          WriteOnly
          (Just 644)
          defaultFileFlags {trunc = True}
      writeNumbers uring fd 1 0
      void $ submit uring
      forM_ [1 .. count] $ \_ -> do
        withCqeData $ cqeWaitAndSee uring
      uring `submitAndComplete` \sqe -> do
        prepFsync sqe fd [DataSync]
      fileShouldHave $ (intercalate "\n" $ map show [1 .. 10]) <> "\n"
      closeFd fd
  where
    writeNumbers :: URing -> Fd -> Int -> Int -> IO ()
    writeNumbers _ _ 11 _ = pure ()
    writeNumbers uring fd n offset = do
      bytesWritten <-
        BS8.useAsCStringLen (BS8.pack (show n <> "\n")) $ \(buf', len) -> do
          submitted <- BS8.packCStringLen (buf', len)
          uring `withSqeData` (n, submitted) $ \sqe ->
            prepWrite sqe fd buf' len offset
          pure len
      writeNumbers uring fd (succ n) (offset + bytesWritten)

--testWriteHigh :: Spec
--testWriteHigh = it "writes using higher level constructs" $ example $ do
--  let string = "hola mundo"
--  withURing $ \uring -> withURingQueue 10 uring [] $ \_ -> do
--    fd <- openFd writeFileName
--                 WriteOnly
--                 Nothing
--                 defaultFileFlags { trunc = True }
--    BS8.useAsCStringLen (BS8.pack string) $ \(buf', len) -> do
--      result <- run uring . setRingData buf' $ mkRingWrite fd buf' len 0
--      (res, readData) <- getResultData result
--      res `shouldBe` length string
--      fileShouldHave string
--    closeFd fd

writeFileName :: String
writeFileName = "test/out.txt"

fileShouldHave :: String -> IO ()
fileShouldHave string = do
  contents <- readFile writeFileName
  contents `shouldBe` string
