{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE CPP #-}

-- Low-level wrappers for liburing
module Control.Concurrent.URing.FFI.Internal
  ( URing
  , URingSq
  , URingSqe
  , URingCqe(res, user_data)
  , foo
  -- , io_uring_get_probe_ring
  -- , io_uring_get_probe
  -- , io_uring_queue_init_params
  , io_uring_queue_init
  , io_uring_foo
  , ringSetupSqPoll
  , ringSetupIoPoll
  , io_uring_sq_space_left
  , io_uring_sq_ready
  , io_uring_cq_ready
  -- , io_uring_queue_mmap
  --, io_uring_ring_dontfork
  , io_uring_submit
  , io_uring_queue_exit
  , io_uring_submit_and_wait
  , io_uring_get_sqe
  , io_uring_sqe_set_data
  , io_uring_sqe_set_flags
  --, io_uring_unregister_buffers
  --, io_uring_unregister_files
  --, io_uring_register_eventfd
  --, io_uring_register_eventfd_async
  --, io_uring_unregister_eventfd
  --, io_uring_register_probe
  --, io_uring_register_personality
  , io_uring_cqe_get_data
  --, io_uring_wait_cqes
  --, io_uring_wait_cqe_timeout
  --, io_uring_register_buffers
  --, io_uring_register_files,
  --, io_uring_register_files_update
  -- inline functions
  , io_uring_opcode_supported
  , io_uring_prep_read
  , io_uring_prep_readv
  , io_uring_prep_nop
  , io_uring_prep_write
  , io_uring_prep_fsync
  , io_uring_prep_connect
  , io_uring_prep_send
  , io_uring_prep_recv
  , io_uring_prep_fallocate
  , io_uring_wait_cqe
  , io_uring_wait_cqe_nr
  , io_uring_cqe_seen
  , io_uring_peek_cqe
  , io_uring_prep_close
  -- defines
  , ioRingFsyncDataSync
  , flKeepSize
  , flUnshare
  , flPunchHole
  , flCollapseRange
  , flZeroRange
  , flInsertRange
  , sqeAsync
  , sqeIoLink
  , sqeIoHardLink
  , sqeIoDrain
  , sqeFixedFile
  )
where

import           Foreign.C.Types
import           Foreign.Ptr
import           Foreign.StablePtr
import           Foreign.Storable (Storable(..))
import Data.Int (Int32)
import Data.Word (Word64, Word32)

import Control.Concurrent.URing.FFI.IOVec

#include <liburing.h>
#include <linux/falloc.h>

-- opaque to struct io_uring_sqe
data URingSqe

data URingSq -- = URingSq
    --{ sq_khead         :: Ptr CUInt
    --, sq_ktail         :: Ptr CUInt
    --, sq_kring_mask    :: Ptr CUInt
    --, sq_kring_entries :: Ptr CUInt
    --, kflags           :: Ptr CUInt
    --, kdropped         :: Ptr CUInt
    --, array            :: Ptr CUInt
    --, sqes             :: Ptr URingSqe
    --, sqe_head         :: Ptr CUInt
    --, sqe_tail         :: Ptr CUInt
    --, sq_ring_sz       :: CSize
    --, sq_ring_ptr      :: Ptr ()
    --}

instance Storable URingSq where
  sizeOf _ = #{size struct io_uring_sq}
  alignment _ = alignment (undefined :: CDouble)
  peek _ = undefined
  poke _ = undefined

-- struct io_uring_cq
data URingCq -- = URingCq
    --{ cq_khead :: Ptr CUInt
    --, cq_ktail :: Ptr CUInt
    --, cq_kring_mask :: Ptr CUInt
    --, cq_kring_entries :: Ptr CUInt
    --, koverflow :: Ptr CUInt
    --, io_uring_cqes :: Ptr URingCqe
    --, cq_ring_sz :: CSize
    --, cq_ring_ptr :: Ptr ()
    --}

instance Storable URingCq where
  sizeOf _ = #{size struct io_uring_cq}
  alignment _ = alignment (undefined :: CDouble)
  peek _ = undefined
  poke _ = undefined

data URing {- = URing
    { sq :: URingSq
    , cq :: URingCq
    , flags :: CUInt
    , ring_fd :: CInt
    }-}

instance Storable URing where
  sizeOf _ = #{size struct io_uring}
  alignment _ = alignment (undefined :: CDouble)
  peek _ = undefined
  poke _ = undefined

-- Opaque data types
data URingCqe = URingCqe
    { user_data :: Word64
    , res       :: Int32
    , flags     :: Word32
    } deriving (Show)

instance Storable URingCqe where
  sizeOf _ = #{size struct io_uring_cqe}
  alignment _ = alignment (undefined :: CDouble)
  peek ptr = do
      user_data' <- (#peek struct io_uring_cqe, user_data) ptr
      res' <- (#peek struct io_uring_cqe, res) ptr
      flags' <- (#peek struct io_uring_cqe, flags) ptr
      pure URingCqe { user_data = user_data', res = res', flags = flags' }
  poke ptr cqe = do
      #{poke struct io_uring_cqe, user_data} ptr (user_data cqe)
      #{poke struct io_uring_cqe, res} ptr (res cqe)
      #{poke struct io_uring_cqe, flags} ptr (flags cqe)

-- struct io_uring_probe
-- data URingProbe

-- struct io_uring_params
-- data URingParams

-- extern struct io_uring_probe *io_uring_get_probe_ring(struct io_uring *ring);
-- foreign import ccall safe "io_uring_get_probe_ring" io_uring_get_probe_ring
--     :: Ptr URing
--     -> IO (Ptr URingProbe)

-- extern struct io_uring_probe *io_uring_get_probe(void);
-- foreign import ccall safe "io_uring_get_probe" io_uring_get_probe
--     :: IO (Ptr URingProbe)

-- extern int io_uring_queue_init_params(unsigned entries, struct io_uring *ring, struct io_uring_params *p);
-- foreign import ccall safe "io_uring_queue_init_params" io_uring_queue_init_params
--     :: CUInt
--     -> Ptr URing
--     -> Ptr URingParams
--     -> IO Int

ringSetupIoPoll :: CUInt
ringSetupIoPoll = #const IORING_SETUP_IOPOLL

ringSetupSqPoll ::  CUInt
ringSetupSqPoll = #const IORING_SETUP_SQPOLL

-- extern int io_uring_queue_init(unsigned entries, struct io_uring *ring, unsigned flags);
-- TODO look into int
foreign import ccall unsafe "io_uring_queue_init" io_uring_queue_init
    :: CUInt
    -> Ptr URing
    -> CUInt
    -> IO CInt

-- TODO fd
-- extern int io_uring_queue_mmap(int fd, struct io_uring_params *p, struct io_uring *ring);
-- foreign import ccall unsafe "io_uring_queue_mmap" io_uring_queue_mmap
--     :: CInt
--     -> Ptr URingParams
--     -> Ptr URing
--     -> IO Int

-- extern int io_uring_ring_dontfork(struct io_uring *ring);
-- foreign import ccall unsafe "io_uring_ring_dontfork" io_uring_ring_dontfork
--     :: Ptr URing
--     -> IO Int

-- extern void io_uring_queue_exit(struct io_uring *ring);
foreign import ccall unsafe "io_uring_queue_exit" io_uring_queue_exit
    :: Ptr URing
    -> IO ()

-- unsigned io_uring_peek_batch_cqe(struct io_uring *ring, struct io_uring_cqe **cqes, unsigned count);
--foreign import ccall unsafe "io_uring_peek_batch_cqe" io_uring_peek_batch_cqe
--    :: Ptr URing
--    -> Ptr (Ptr URingCqe)
--    -> CUInt
--    -> IO CUInt

--extern int io_uring_wait_cqes(struct io_uring *ring,
--	struct io_uring_cqe **cqe_ptr, unsigned wait_nr,
--	struct __kernel_timespec *ts, sigset_t *sigmask);

--extern int io_uring_wait_cqe_timeout(struct io_uring *ring,
--	struct io_uring_cqe **cqe_ptr, struct __kernel_timespec *ts);

-- extern int io_uring_submit(struct io_uring *ring);
foreign import ccall unsafe "io_uring_submit" io_uring_submit
    :: Ptr URing
    -> IO CInt

-- extern int io_uring_submit_and_wait(struct io_uring *ring, unsigned wait_nr);
foreign import ccall unsafe "io_uring_submit_and_wait" io_uring_submit_and_wait
    :: Ptr URing
    -> CUInt
    -> IO CInt

-- extern struct io_uring_sqe *io_uring_get_sqe(struct io_uring *ring);
foreign import ccall unsafe "io_uring_get_sqe" io_uring_get_sqe
    :: Ptr URing
    -> IO (Ptr URingSqe)

--extern int io_uring_register_buffers(struct io_uring *ring,
--					const struct iovec *iovecs,
--					unsigned nr_iovecs);

-- extern int io_uring_unregister_buffers(struct io_uring *ring);
-- foreign import ccall unsafe "io_uring_unregister_buffers" io_uring_unregister_buffers
--     :: Ptr URing
--     -> IO Int

-- extern int io_uring_register_files(struct io_uring *ring, const int *files,
--					unsigned nr_files);

--extern int io_uring_unregister_files(struct io_uring *ring);
-- foreign import ccall unsafe "io_uring_unregister_files" io_uring_unregister_files
--     :: Ptr URing
--     -> IO Int

-- extern int io_uring_register_files_update(struct io_uring *ring, unsigned off,
--					int *files, unsigned nr_files);

-- extern int io_uring_register_eventfd(struct io_uring *ring, int fd);
-- foreign import ccall unsafe "io_uring_register_eventfd" io_uring_register_eventfd
--     :: Ptr URing
--     -> CInt
--     -> IO Int

-- extern int io_uring_register_eventfd_async(struct io_uring *ring, int fd);
-- foreign import ccall unsafe "io_uring_register_eventfd_async" io_uring_register_eventfd_async
--     :: Ptr URing
--     -> CInt
--     -> IO Int

-- extern int io_uring_unregister_eventfd(struct io_uring *ring);
-- foreign import ccall unsafe "io_uring_unregister_eventfd" io_uring_unregister_eventfd
--     :: Ptr URing
--     -> IO Int

-- extern int io_uring_register_probe(struct io_uring *ring,
--					struct io_uring_probe *p, unsigned nr);
-- foreign import ccall unsafe "io_uring_register_probe" io_uring_register_probe
--     :: Ptr URing
--     -> Ptr URingProbe
--     -> IO Int

-- extern int io_uring_register_personality(struct io_uring *ring);
--foreign import ccall unsafe "io_uring_register_personality" io_uring_register_personality
--    :: Ptr URing
--     -> IO Int

-- extern int io_uring_unregister_personality(struct io_uring *ring, int id);
-- foreign import ccall unsafe "io_uring_unregister_personality" io_uring_unregister_personality
--     :: Ptr URing
--     -> CInt
--     -> IO Int

-- inline functions
foreign import ccall unsafe "uring.c opcode_supported" io_uring_opcode_supported
    :: Ptr URing
    -> CInt
    -> IO CInt

-- TODO how to handle inline functions?
-- static inline int io_uring_opcode_supported(struct io_uring_probe *p, int op)
-- {
-- 	if (op > p->last_op)
-- 		return 0;
-- 	return (p->ops[op].flags & IO_URING_OP_SUPPORTED) != 0;
-- }
--
-- /*
-- * Helper for the peek/wait single cqe functions. Exported because of that,
-- * but probably shouldn't be used directly in an application.
-- */
--extern int __io_uring_get_cqe(struct io_uring *ring,
--			      struct io_uring_cqe **cqe_ptr, unsigned submit,
--			      unsigned wait_nr, sigset_t *sigmask);
--
-- #define LIBURING_UDATA_TIMEOUT	((__u64) -1)
--
-- #define io_uring_for_each_cqe(ring, head, cqe)				\
--	/*								\
--	 * io_uring_smp_load_acquire() enforces the order of tail	\
--	 * and CQE reads.						\
--	 */								\
--	for (head = *(ring)->cq.khead;					\
--	     (cqe = (head != io_uring_smp_load_acquire((ring)->cq.ktail) ? \
--		&(ring)->cq.cqes[head & (*(ring)->cq.kring_mask)] : NULL)); \
--	     head++)							\
--
-- /*
-- * Must be called after io_uring_for_each_cqe()
-- */
--static inline void io_uring_cq_advance(struct io_uring *ring,
--				       unsigned nr)
--{
--	if (nr) {
--		struct io_uring_cq *cq = &ring->cq;
--
--		/*
--		 * Ensure that the kernel only sees the new value of the head
--		 * index after the CQEs have been read.
--		 */
--		io_uring_smp_store_release(cq->khead, *cq->khead + nr);
--	}
--}
--
-- /*
-- * Must be called after io_uring_{peek,wait}_cqe() after the cqe has
-- * been processed by the application.
-- */
--static inline void io_uring_cqe_seen(struct io_uring *ring,
--				     struct io_uring_cqe *cqe)
foreign import ccall unsafe "uring.c cqe_seen" io_uring_cqe_seen
    :: Ptr URing
    -> Ptr URingCqe
    -> IO ()

foreign import ccall unsafe "uring.h foo" io_uring_foo
    :: Ptr URing
    -> CInt
    -> IO CInt

-- static inline void io_uring_sqe_set_data(struct io_uring_sqe *sqe, void *data)
foreign import ccall unsafe "uring.c sqe_set_data" io_uring_sqe_set_data
    :: Ptr URingSqe
    -> Ptr a
    -> IO ()

-- static inline void *io_uring_cqe_get_data(const struct io_uring_cqe *cqe)
foreign import ccall unsafe "uring.c cqe_get_data" io_uring_cqe_get_data
    :: Ptr URingCqe
    -> IO (Ptr a)

sqeFixedFile :: CUInt
sqeFixedFile = #const IOSQE_FIXED_FILE

sqeIoDrain :: CUInt
sqeIoDrain = #const IOSQE_IO_DRAIN

sqeIoLink :: CUInt
sqeIoLink = #const IOSQE_IO_LINK

sqeIoHardLink :: CUInt
sqeIoHardLink = #const IOSQE_IO_HARDLINK

sqeAsync :: CUInt
sqeAsync = #const IOSQE_ASYNC

-- static inline void io_uring_sqe_set_flags(struct io_uring_sqe *sqe, unsigned flags)
foreign import ccall unsafe "uring.c sqe_set_flags" io_uring_sqe_set_flags
    :: Ptr URingSqe
    -> CUInt
    -> IO ()
--
--static inline void io_uring_prep_rw(int op, struct io_uring_sqe *sqe, int fd,
--				    const void *addr, unsigned len,
--				    __u64 offset)
--static inline void io_uring_prep_splice(struct io_uring_sqe *sqe,
--					int fd_in, loff_t off_in,
--					int fd_out, loff_t off_out,
--					unsigned int nbytes,
--					unsigned int splice_flags)
--static inline void io_uring_prep_readv(struct io_uring_sqe *sqe, int fd,
--				       const struct iovec *iovecs,
--				       unsigned nr_vecs, off_t offset)
foreign import ccall unsafe "uring.c prep_readv" io_uring_prep_readv
    :: Ptr URingSqe
    -> CInt
    -> Ptr IOVec -- array of iovec
    -> CInt -- number of iovec
    -> CInt -- offset
    -> IO () 
--static inline void io_uring_prep_read_fixed(struct io_uring_sqe *sqe, int fd,
--					    void *buf, unsigned nbytes,
--					    off_t offset, int buf_index)
--static inline void io_uring_prep_writev(struct io_uring_sqe *sqe, int fd,
--					const struct iovec *iovecs,
--					unsigned nr_vecs, off_t offset)
--static inline void io_uring_prep_write_fixed(struct io_uring_sqe *sqe, int fd,
--					     const void *buf, unsigned nbytes,
--					     off_t offset, int buf_index)
--static inline void io_uring_prep_recvmsg(struct io_uring_sqe *sqe, int fd, struct msghdr *msg, unsigned flags)
--static inline void io_uring_prep_sendmsg(struct io_uring_sqe *sqe, int fd, const struct msghdr *msg,
--                   unsigned flags)
--static inline void io_uring_prep_poll_add(struct io_uring_sqe *sqe, int fd, short poll_mask)
--static inline void io_uring_prep_poll_remove(struct io_uring_sqe *sqe, void *user_data)

--static inline void io_uring_prep_fsync(struct io_uring_sqe *sqe, int fd, unsigned fsync_flags)
ioRingFsyncDataSync :: CInt
ioRingFsyncDataSync = #const IORING_FSYNC_DATASYNC

foreign import ccall unsafe "uring.h prep_fsync" io_uring_prep_fsync
    :: Ptr URingSqe
    -> CInt
    -> CInt
    -> IO ()
--static inline void io_uring_prep_nop(struct io_uring_sqe *sqe)
--static inline void io_uring_prep_timeout(struct io_uring_sqe *sqe,
--					 struct __kernel_timespec *ts,
--					 unsigned count, unsigned flags)
--static inline void io_uring_prep_timeout_remove(struct io_uring_sqe *sqe,
--						__u64 user_data, unsigned flags)
--static inline void io_uring_prep_accept(struct io_uring_sqe *sqe, int fd,
--					struct sockaddr *addr,
--					socklen_t *addrlen, int flags)
--static inline void io_uring_prep_cancel(struct io_uring_sqe *sqe, void *user_data,
--					int flags)
--static inline void io_uring_prep_link_timeout(struct io_uring_sqe *sqe,
--					      struct __kernel_timespec *ts,
--					      unsigned flags)

--static inline void io_uring_prep_connect(struct io_uring_sqe *sqe, int fd,
--					 struct sockaddr *addr,
--					 socklen_t addrlen)
foreign import ccall unsafe "uring.c prep_connect" io_uring_prep_connect
    :: Ptr URingSqe
    -> CInt
    -> StablePtr s --TODO
    -> CInt
    -> IO ()
--static inline void io_uring_prep_files_update(struct io_uring_sqe *sqe,
--					      int *fds, unsigned nr_fds,
--					      int offset)
--static inline void io_uring_prep_fallocate(struct io_uring_sqe *sqe, int fd,
--					   int mode, off_t offset, off_t len)

-- flags from linux/falloc.h
flKeepSize :: CInt
flKeepSize = #const FALLOC_FL_KEEP_SIZE

flUnshare :: CInt
flUnshare = #const FALLOC_FL_UNSHARE_RANGE

flPunchHole :: CInt
flPunchHole = #const FALLOC_FL_PUNCH_HOLE

flCollapseRange :: CInt
flCollapseRange = #const FALLOC_FL_COLLAPSE_RANGE

flZeroRange :: CInt
flZeroRange = #const FALLOC_FL_ZERO_RANGE

flInsertRange :: CInt
flInsertRange = #const FALLOC_FL_INSERT_RANGE

foreign import ccall unsafe "uring.c prep_fallocate" io_uring_prep_fallocate
    :: Ptr URingSqe
    -> CInt
    -> CInt
    -> CInt
    -> CInt
    -> IO ()

--static inline void io_uring_prep_openat(struct io_uring_sqe *sqe, int dfd,
--					const char *path, int flags, mode_t mode)
--static inline void io_uring_prep_close(struct io_uring_sqe *sqe, int fd)
foreign import ccall unsafe "uring.h prep_close" io_uring_prep_close
    :: Ptr URingSqe
    -> CInt
    -> IO ()

--static inline void io_uring_prep_read(struct io_uring_sqe *sqe, int fd,
--				      void *buf, unsigned nbytes, off_t offset)
foreign import ccall unsafe "uring.h prep_read" io_uring_prep_read
    :: Ptr URingSqe
    -> CInt
    -> Ptr a
    -> CUInt
    -> CInt
    -> IO ()

--static inline void io_uring_prep_write(struct io_uring_sqe *sqe, int fd,
--				       const void *buf, unsigned nbytes, off_t offset);
foreign import ccall unsafe "uring.h prep_write" io_uring_prep_write
    :: Ptr URingSqe
    -> CInt
    -> Ptr a
    -> CUInt
    -> CInt
    -> IO ()

--struct statx;
--static inline void io_uring_prep_statx(struct io_uring_sqe *sqe, int dfd,
--				const char *path, int flags, unsigned mask,
--				struct statx *statxbuf)
--static inline void io_uring_prep_fadvise(struct io_uring_sqe *sqe, int fd,
--					 off_t offset, off_t len, int advice)
--static inline void io_uring_prep_madvise(struct io_uring_sqe *sqe, void *addr,
--					 off_t length, int advice)

--static inline void io_uring_prep_send(struct io_uring_sqe *sqe, int sockfd,
--				      const void *buf, size_t len, int flags)
foreign import ccall unsafe "uring.c prep_send" io_uring_prep_send
    :: Ptr URingSqe
    -> CInt
    -> Ptr a
    -> CSize
    -> CInt
    -> IO ()

--static inline void io_uring_prep_recv(struct io_uring_sqe *sqe, int sockfd,
--				      void *buf, size_t len, int flags)
foreign import ccall unsafe "uring.c prep_recv" io_uring_prep_recv
    :: Ptr URingSqe
    -> CInt
    -> Ptr a
    -> CSize
    -> CInt
    -> IO ()
--
--static inline void io_uring_prep_openat2(struct io_uring_sqe *sqe, int dfd,
--					const char *path, struct open_how *how)
--struct epoll_event;
--static inline void io_uring_prep_epoll_ctl(struct io_uring_sqe *sqe, int epfd,
--					   int fd, int op,
--					   struct epoll_event *ev)
--static inline void io_uring_prep_provide_buffers(struct io_uring_sqe *sqe,
--						 void *addr, int len, int nr,
--						 int bgid, int bid)
--static inline void io_uring_prep_remove_buffers(struct io_uring_sqe *sqe,
--						int nr, int bgid)

--static inline unsigned io_uring_sq_ready(struct io_uring *ring)
foreign import ccall unsafe "uring.h sq_ready" io_uring_sq_ready
    :: Ptr URing
    -> IO CInt

--static inline unsigned io_uring_sq_space_left(struct io_uring *ring)
foreign import ccall unsafe "uring.h sq_space_left" io_uring_sq_space_left
    :: Ptr URing
    -> CInt

--static inline unsigned io_uring_cq_ready(struct io_uring *ring)
foreign import ccall unsafe "uring.h cq_ready" io_uring_cq_ready
    :: Ptr URing
    -> IO CInt
-- /*
-- * Return an IO completion, waiting for 'wait_nr' completions if one isn't
-- * readily available. Returns 0 with cqe_ptr filled in on success, -errno on
-- * failure.
-- */
--static inline int io_uring_wait_cqe_nr(struct io_uring *ring,
--				      struct io_uring_cqe **cqe_ptr,
--				      unsigned wait_nr)
foreign import ccall unsafe "uring.h wait_cqe_nr" io_uring_wait_cqe_nr
    :: Ptr URing
    -> Ptr URingCqe
    -> CInt
    -> IO CInt

foreign import ccall unsafe "uring.h prep_nop" io_uring_prep_nop
    :: Ptr URingSqe
    -> IO ()

-- /*
-- * Return an IO completion, if one is readily available. Returns 0 with
-- * cqe_ptr filled in on success, -errno on failure.
-- */
--static inline int io_uring_peek_cqe(struct io_uring *ring,
--				    struct io_uring_cqe **cqe_ptr)
foreign import ccall unsafe "uring.h peek_cqe" io_uring_peek_cqe
    :: Ptr URing
    -> Ptr (Ptr URingCqe)
    -> IO CInt
-- /*
-- * Return an IO completion, waiting for it if necessary. Returns 0 with
-- * cqe_ptr filled in on success, -errno on failure.
-- */
--static inline int io_uring_wait_cqe(struct io_uring *ring,
--				    struct io_uring_cqe **cqe_ptr)
foreign import ccall unsafe "uring.h wait_cqe" io_uring_wait_cqe
    :: Ptr URing
    -> Ptr (Ptr URingCqe)
    -> IO CInt

foreign import ccall unsafe "uring.h foo" foo
    :: Ptr URing
    -> Ptr (URingCqe)
    -> CInt
    -> IO (Ptr URingCqe)
