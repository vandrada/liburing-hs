module Control.Concurrent.URing.URing
  ( URing,
    URingFlag (IOPoll, SqPoll),
    withURing,
    submit,
    isFull,
    submitAndWait,
    sqSpaceLeft,
    sqReady,
    cqReady,
    -- uring queue
    uringQueueInit,
    uringQueueExit,
    withURingQueue,
    -- opcode
    opcodeSupported,
  )
where

import qualified Control.Concurrent.URing.FFI.Internal as FFI
import Control.Concurrent.URing.General
  ( collapseFlags,
    mkErrno,
    throwUnless,
  )
import Control.Exception
  ( Exception,
    bracket_,
  )
import Control.Monad.Catch (MonadThrow)
import Control.Monad.IO.Class
  ( MonadIO,
    liftIO,
  )
import Foreign.C.Types (CUInt)
import Foreign.Marshal.Alloc (alloca)
import Foreign.Marshal.Utils (toBool)
import Foreign.Ptr (Ptr)

type URing = Ptr FFI.URing

newtype URingException = FailedToInitQueue Int deriving (Show)

instance Exception URingException

withURing :: (MonadIO m) => (URing -> IO a) -> m a
withURing = liftIO . alloca

data URingFlag
  = IOPoll -- perform busy waiting for an I/O completion
  | SqPoll -- spawn a kernel thread to perform polling. I/O requires no context switches

uringFlagsToCInt :: URingFlag -> CUInt
uringFlagsToCInt IOPoll = FFI.ringSetupIoPoll
uringFlagsToCInt SqPoll = FFI.ringSetupSqPoll

-- | submit work to the ring. Returns the number of sqes submitted
submit :: (MonadIO m) => URing -> m Int
{-# INLINE submit #-}
submit = liftIO . fmap fromIntegral . FFI.io_uring_submit

-- | submit work and wait for completion in a single system call. Returns the
-- number of sqes submitted
submitAndWait :: (MonadIO m) => URing -> Int -> m Int
submitAndWait uring timeout =
  liftIO $
    fromIntegral
      <$> FFI.io_uring_submit_and_wait
        uring
        (fromIntegral timeout)

-- | initialize a queue with a depth of @entries. @flags can be used to control
-- the rings behavior. See @URingFlag for more info
uringQueueInit ::
  (MonadIO m, MonadThrow m) => Int -> URing -> [URingFlag] -> m ()
{-# INLINE uringQueueInit #-}
uringQueueInit entries uring flags = do
  ret <-
    liftIO $
      FFI.io_uring_queue_init
        (fromIntegral entries)
        uring
        (collapseFlags uringFlagsToCInt flags)
  throwUnless (ret == 0) (FailedToInitQueue . mkErrno $ ret)

-- | exit the queue
uringQueueExit :: (MonadIO m) => URing -> m ()
uringQueueExit = liftIO . FFI.io_uring_queue_exit

-- | initializes a URing queue, performs the passed in work, and exits on
-- completion. See @uringQueueInit for details on @entries and @flags
withURingQueue ::
  (MonadIO m) => Int -> URing -> [URingFlag] -> (URing -> IO a) -> m a
{-# INLINE withURingQueue #-}
withURingQueue entries uring flags action =
  liftIO $
    bracket_
      (uringQueueInit entries uring flags)
      (uringQueueExit uring)
      (action uring)

opcodeSupported :: (MonadIO m) => URing -> Int -> m Bool
opcodeSupported uring opcode =
  liftIO $ toBool <$> FFI.io_uring_opcode_supported uring (fromIntegral opcode)

-- | Returns the number of free spaces in the submission queue
sqSpaceLeft :: URing -> Int
sqSpaceLeft = fromIntegral . FFI.io_uring_sq_space_left

-- | Returns the number of submissions ready
sqReady :: (MonadIO m) => URing -> m Int
sqReady = liftIO . fmap fromIntegral . FFI.io_uring_sq_ready

cqReady :: (MonadIO m) => URing -> m Int
cqReady = liftIO . fmap fromIntegral . FFI.io_uring_cq_ready

isFull :: URing -> Bool
isFull uring = sqSpaceLeft uring == 0
