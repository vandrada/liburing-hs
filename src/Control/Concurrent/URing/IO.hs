{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Control.Concurrent.URing.IO
  ( RingHandle,
    Completion (userData, completionResult),
    RingSubmission,
    RingM,
    IOVec.IOVec (..),
    queueFull,
    withRing,
    complete_,
    drain,
    setUserData,
    withUserData,
    withQueue,
    mkRing,
    submitRing,
    uRead,
    uReadV,
    uWrite,
    uClose,
    complete,
    completeNr,
    completeNr_,
  )
where

import qualified Control.Concurrent.URing.Cqe as Cqe
import qualified Control.Concurrent.URing.FFI.IOVec as IOVec
import Control.Concurrent.URing.General
  ( mkErrno,
  )
import Control.Concurrent.URing.Prep
  ( prepClose,
    prepRead,
    prepReadV,
    prepWrite,
  )
import qualified Control.Concurrent.URing.Sqe as Sqe
import Control.Concurrent.URing.URing
  ( URing,
    withURing,
    withURingQueue,
  )
import qualified Control.Concurrent.URing.URing as U
import Control.Monad
  ( forM,
    replicateM,
    when,
  )
import Control.Monad.Catch
  ( Exception,
    MonadThrow,
    throwM,
  )
import Control.Monad.IO.Class
  ( MonadIO,
    liftIO,
  )
import Control.Monad.IO.Unlift (MonadIO (..), MonadUnliftIO)
import Control.Monad.Reader (MonadReader (ask), ReaderT (..), when)
import qualified Data.ByteString as BS
import Data.Int (Int32)
import Foreign (Int32, Ptr)
import Foreign.Storable (peek)
import System.Posix.Types (Fd)

data Queue = Queue {_submitQ :: (), _completeQ :: ()}

queueFull :: RingM Bool
queueFull = do
  ring <- ask
  pure $ U.isFull (uring ring)

-- TODO rename after removing the old `Ring`
data RingHandle = RingHandle
  { uring :: !URing
  }
  deriving (Show)

-- | A piece of completed work from uring
data Completion a = Completion
  { userData :: !(Maybe a),
    completionResult :: !Int32,
    cqePtr :: Ptr Cqe.URingCqe
  }
  deriving (Show)

instance Functor Completion where
  fmap f completion = completion {userData = fmap f (userData completion)}

-- | RingM monad
newtype RingM a = RingM {unRingM :: ReaderT RingHandle IO a}
  deriving (Functor, Applicative, Monad, MonadIO, MonadThrow, MonadReader RingHandle, MonadUnliftIO)

-- | A piece of work to submit to uring
-- TODO rename
data RingSubmission a = RingSubmission
  { sqe :: !(Sqe.Sqe a)
  }

setUserData :: RingSubmission () -> a -> RingM (RingSubmission a)
setUserData submission userData' = do
  sqe' <- liftIO $ Sqe.sqeSetData (sqe submission) userData'
  pure (RingSubmission sqe')

withUserData :: a -> RingSubmission () -> RingM (RingSubmission a)
withUserData = flip setUserData

runRingM :: RingHandle -> RingM a -> IO a
runRingM handle ringM = runReaderT (unRingM ringM) handle

mkCompletion :: (MonadIO m) => Cqe.Cqe -> m (Completion a)
mkCompletion cqe = Completion <$> Cqe.cqeGetData cqe <*> Cqe.cqeResult cqe <*> Cqe.getPtr cqe

mkCompletion' :: (MonadIO m) => Cqe.Cqe' a -> m (Completion a)
mkCompletion' cqe = Completion <$> pure (Cqe._data cqe) <*> pure (Cqe.res cqe) <*> Cqe.getPtr' cqe

mkRing :: URing -> RingHandle
mkRing = RingHandle

-- TODO remove?
withRing :: (URing -> IO a) -> IO a
{-# INLINE withRing #-}
withRing = withURing

-- TODO add options
withQueue :: URing -> Int -> (Queue -> RingM a) -> IO a
{-# INLINE withQueue #-}
withQueue ring depth action = do
  withURingQueue depth ring [] $
    \_ -> runRingM (mkRing ring) $ action (Queue () ())

submitRing :: RingM Int
submitRing = do
  ring <- ask
  U.submit (uring ring)

uRead :: Ptr a -> Int -> Fd -> Int -> RingM (RingSubmission ())
{-# INLINE uRead #-}
uRead buf len fd offset = do
  sqe' <- getSqe
  liftIO $ prepRead sqe' fd buf len offset
  pure $ RingSubmission sqe'

uWrite :: BS.ByteString -> Fd -> Int -> RingM ()
{-# INLINE uWrite #-}
uWrite bs fd offset = do
  sqe' <- getSqe
  liftIO . BS.useAsCStringLen bs $ \(buf, len) -> do
    prepWrite sqe' fd buf len offset
    pure ()

uReadV :: Fd -> Ptr IOVec.IOVec -> Int -> Int -> RingM (RingSubmission ())
uReadV fd ioVecs numVecs offset = do
  sqe' <- getSqe
  liftIO $ prepReadV sqe' fd ioVecs numVecs offset
  pure $ RingSubmission sqe'

uClose :: Fd -> RingM (RingSubmission ())
uClose fd = do
  sqe' <- getSqe
  liftIO $ prepClose sqe' fd
  pure $ RingSubmission sqe'

getSqe :: RingM (Sqe.Sqe ())
{-# INLINE getSqe #-}
getSqe = do
  ring <- ask
  Sqe.getSqe (uring ring)

newtype RingException = RingException Int deriving (Show)

instance Exception RingException

-- | complete a number of submissions
complete :: Int -> RingM [Completion a]
{-# INLINE complete #-}
complete n = completeCqes n mkCompletion

-- | complete the number of submissions in one syscall
completeNr :: Int -> (Completion a -> RingM b) -> RingM [b]
completeNr n f = do
  ring <- ask
  completions <- Cqe.cqeWaitNr (uring ring) n mkCompletion'
  forM completions $ \completion -> do
    res <- f completion
    Cqe.cqeSeen' (uring ring) $ cqePtr completion
    pure res

completeNr_ :: Int -> RingM ()
completeNr_ n = do
  res <- completeNr n (\compe -> pure ())
  pure ()

complete_ :: Int -> RingM ()
complete_ n = do
  completeCqes n (\a -> pure ())
  pure ()

-- TODO do this in one syscall, io_uring_wait_cqe_nr?
completeCqes :: Int -> (Cqe.Cqe -> IO a) -> RingM [a]
{-# INLINE completeCqes #-}
completeCqes n f = do
  ring <- ask
  completions <- liftIO $ go ring n []
  pure $ reverse completions
  where
    -- this function waits for a cqe and marks it as seen, letting the kernel
    -- clean it up
    go _ 0 acc = pure acc
    go ring n acc = Cqe.withCqe $ \cqe -> do
      Cqe.waitCqe (uring ring) cqe
      res <- Cqe.cqeResult cqe
      when (res < 0) $ throwM (RingException . mkErrno $ fromIntegral res)
      result <- f cqe
      Cqe.cqeSeen (uring ring) cqe
      go ring (n - 1) (result : acc)

drain :: RingM [Completion a]
drain = do
  ring <- ask
  available <- U.cqReady (uring ring)
  completeCqes available mkCompletion
