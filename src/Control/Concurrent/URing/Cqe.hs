module Control.Concurrent.URing.Cqe
  ( waitCqe,
    Cqe' (..),
    getPtr,
    getPtr',
    cqeSeen,
    peekCqe,
    withCqe,
    withCqeData,
    cqeSeen',
    cqeWaitAndSee,
    cqeGetData,
    cqeResult,
    cqeWaitNr,
    Cqe,
    FFI.URingCqe,
  )
where

--, cqReady

import qualified Control.Concurrent.URing.FFI.Internal as FFI
import Control.Concurrent.URing.URing
  ( URing,
  )
import Control.Exception (Exception)
import Control.Monad
  ( forM,
    forM_,
    replicateM,
    unless,
    void,
    when,
  )
import Control.Monad.Catch
  ( MonadThrow,
    throwM,
  )
import Control.Monad.IO.Class
  ( MonadIO,
    liftIO,
  )
import Data.Int (Int32)
import Foreign.Marshal.Alloc (alloca, allocaBytes)
import Foreign.Marshal.Array
  ( allocaArray,
    peekArray,
  )
import Foreign.Marshal.Utils (with)
import Foreign.Ptr
  ( Ptr,
    castPtr,
    nullPtr,
    plusPtr,
  )
import Foreign.StablePtr
  ( StablePtr,
    castPtrToStablePtr,
    castStablePtrToPtr,
    deRefStablePtr,
    freeStablePtr,
  )
import Foreign.Storable (peek, peekElemOff, poke)

newtype Cqe = Cqe
  { ptr :: Ptr (Ptr FFI.URingCqe)
  }
  deriving (Show)

getPtr :: MonadIO m => Cqe -> m (Ptr FFI.URingCqe)
getPtr = liftIO . peek . ptr

data Cqe' a = Cqe'
  { _data :: Maybe a,
    res :: Int32,
    ptr' :: Ptr FFI.URingCqe,
    dataPtr :: Ptr ()
  }

getPtr' :: MonadIO m => Cqe' a -> m (Ptr FFI.URingCqe)
getPtr' = pure . ptr'

newtype CqeException = CqeException Int deriving (Show)

instance Exception CqeException

-- | aquires a cqe and frees it and the end of a computation
withCqe :: (MonadIO m) => (Cqe -> IO c) -> m c
{-# INLINE withCqe #-}
withCqe f = liftIO . alloca $ \p -> do
  f (Cqe p)

-- | similar to @withCqe, but returns user_data on exit. @action _must_ call
-- either cqeWait or cqePeek to have the kernel fill in the cqe
withCqeData :: (MonadIO m) => (Cqe -> IO a) -> m (Maybe b)
withCqeData action = withCqe $ \cqe -> do
  void $ action cqe
  cqeGetData cqe

-- | Waits for io completion. Fills in @cqe on success
waitCqe :: (MonadThrow m, MonadIO m) => URing -> Cqe -> m ()
{-# INLINE waitCqe #-}
waitCqe uring cqe = do
  ret <- liftIO $ fromIntegral <$> FFI.io_uring_wait_cqe uring (ptr cqe)
  unless (ret == 0) $ throwM (CqeException ret)

-- | Returns the completed IO action if one is ready
peekCqe :: (MonadIO m, MonadThrow m) => URing -> Cqe -> m ()
peekCqe uring cqe = do
  ret <- liftIO $ fromIntegral <$> FFI.io_uring_peek_cqe uring (ptr cqe)
  unless (ret == 0) $ throwM (CqeException ret)

-- | advances the completion queue, must be called after @waitCqe and @peekCqe
cqeSeen :: (MonadIO m) => URing -> Cqe -> m ()
{-# INLINE cqeSeen #-}
cqeSeen uring cqe = liftIO $ do
  cqe' <- peek (ptr cqe)
  ptr' <- cqeGetData' cqe
  when (ptr' /= nullPtr) $ do
    freeStablePtr $ castPtrToStablePtr ptr'
  FFI.io_uring_cqe_seen uring cqe'

cqeSeen' :: (MonadIO m) => URing -> Ptr FFI.URingCqe -> m ()
{-# INLINE cqeSeen' #-}
cqeSeen' uring cqe = liftIO $ do
  -- free the mayb user data
  userData <- FFI.io_uring_cqe_get_data cqe
  when (userData /= nullPtr) $ do
    freeStablePtr $ castPtrToStablePtr userData
  FFI.io_uring_cqe_seen uring cqe

-- | returns the result of the completed action, corresponds to `cqe->res` in C
cqeResult :: (MonadIO m) => Cqe -> m Int32
{-# INLINE cqeResult #-}
cqeResult cqe = liftIO $ FFI.res <$> (peek (ptr cqe) >>= peek)

-- | waits for the completion queue entry and advances the completion queue
cqeWaitAndSee :: (MonadIO m) => URing -> Cqe -> m ()
cqeWaitAndSee uring cqe = liftIO $ waitCqe uring cqe >> cqeSeen uring cqe

cqeGetData' :: (MonadIO m) => Cqe -> m (Ptr a)
{-# INLINE cqeGetData' #-}
cqeGetData' cqe = liftIO $ do
  cqe' <- peek (ptr cqe)
  FFI.io_uring_cqe_get_data cqe'

cqeGetData :: (MonadIO m) => Cqe -> m (Maybe a)
{-# INLINE cqeGetData #-}
cqeGetData cqe = do
  ptr' <- cqeGetData' cqe
  lookupData ptr'

--if ptr' == nullPtr
--  then pure Nothing
--  else do
--    val <- liftIO $ deRefStablePtr $ castPtrToStablePtr ptr'
--    pure (Just val)

cqeWaitNr :: (MonadIO m) => URing -> Int -> (Cqe' b -> IO a) -> m [a]
{-# INLINE cqeWaitNr #-}
cqeWaitNr uring n f = do
  liftIO $
    alloca $ \cqe -> do
      cqe' <- liftIO $ FFI.foo uring cqe (fromIntegral n)
      cs <- peekArray n cqe'
      forM cs $ \cqe -> do
        with cqe $ \dumb -> do
          userData <- FFI.io_uring_cqe_get_data dumb
          userData' <- lookupData userData
          f $ Cqe' userData' (FFI.res cqe) dumb userData

lookupData :: (MonadIO m) => Ptr () -> m (Maybe a)
{-# INLINE lookupData #-}
lookupData ptr =
  if ptr == nullPtr
    then pure Nothing
    else do
      val <- liftIO $ deRefStablePtr $ castPtrToStablePtr ptr
      pure (Just val)
