{-# LANGUAGE RankNTypes #-}

module Control.Concurrent.URing.Sqe
  ( Sqe (ptr),
    getSqe,
    sqeSetData,
    withSqe,
    withSqe',
    withSqeData,
    submitAndComplete,
    sqeSetFlags,
    SqeFlag (FixedFile, Drain, Link, Hardlink, Async),
  )
where

import Control.Concurrent.URing.Cqe
  ( cqeSeen,
    waitCqe,
    withCqe,
  )
import qualified Control.Concurrent.URing.FFI.Internal as FFI
import Control.Concurrent.URing.General
  ( collapseFlags,
  )
import Control.Concurrent.URing.URing
  ( URing,
    submit,
    submitAndWait,
  )
import Control.Exception (Exception)
import Control.Monad
  ( void,
    when,
  )
import Control.Monad.Catch
  ( MonadThrow,
    throwM,
  )
import Control.Monad.IO.Class
  ( MonadIO,
    liftIO,
  )
import Foreign.C.Types (CUInt)
import Foreign.Ptr
  ( Ptr,
    nullPtr,
  )
import Foreign.StablePtr
  ( StablePtr,
    castStablePtrToPtr,
    newStablePtr,
  )

newtype Sqe a = Sqe
  { ptr :: Ptr FFI.URingSqe
  }

data SqeException = SqQueueFull deriving (Show)

instance Exception SqeException

data SqeFlag
  = -- | treat fd as an index into files registere with io_uring_register
    FixedFile
  | -- | wait for previous SQEs to complete
    Drain
  | -- | link with the next SQE, used to form a chain of SQEs
    Link
  | -- | similar to @Link, but doesn't sever the chain
    Hardlink
  | -- | ask for an sqe to be issued async from the start
    Async
  deriving (Show)

-- | get a submission queue entry, there should be one-to-one correspondence
-- between a sqe and a cqe. If getting a sqe falies, e.g the queue is full,
-- throws SqQueueFull
getSqe :: (MonadIO m, MonadThrow m) => URing -> m (Sqe ())
{-# INLINE getSqe #-}
getSqe uring = do
  sqe <- liftIO $ FFI.io_uring_get_sqe uring
  when (sqe == nullPtr) $ throwM SqQueueFull
  pure $ Sqe sqe

sqeSetFlags :: (MonadIO m, MonadThrow m) => Sqe a -> [SqeFlag] -> m ()
sqeSetFlags sqe flags =
  liftIO $
    FFI.io_uring_sqe_set_flags (ptr sqe) (collapseFlags sqeFlagToCInt flags)

sqeFlagToCInt :: SqeFlag -> CUInt
sqeFlagToCInt FixedFile = FFI.sqeFixedFile
sqeFlagToCInt Drain = FFI.sqeIoDrain
sqeFlagToCInt Link = FFI.sqeIoLink
sqeFlagToCInt Hardlink = FFI.sqeIoHardLink
sqeFlagToCInt Async = FFI.sqeAsync

sqeSetData' :: (MonadIO m) => Sqe b -> Ptr a -> m ()
sqeSetData' sqe = liftIO . FFI.io_uring_sqe_set_data (ptr sqe)

-- | Set associated data with an @Sqe, the data can be retrieved with
-- @cqeGetData
sqeSetData :: (MonadIO m) => Sqe b -> a -> m (Sqe a)
{-# INLINE sqeSetData #-}
sqeSetData sqe customData = do
  ptr' <- liftIO $ newStablePtr customData
  sqeSetData' sqe (castStablePtrToPtr ptr')
  pure (Sqe (ptr sqe))

-- | acquires a submission queue entry, applies it to @action, and submits the
-- entry
withSqe :: (MonadIO m, MonadThrow m) => URing -> (Sqe () -> m a) -> m a
withSqe uring action = do
  sqe <- getSqe uring
  action sqe

-- | acquires a submission queue entry, applies it to @action, submits the
-- entry and waits for completion
withSqe' :: (MonadIO m, MonadThrow m) => URing -> (Sqe () -> m a) -> Int -> m a
withSqe' uring action timeout = do
  sqe <- getSqe uring
  ret <- action sqe
  void $ submitAndWait uring timeout
  pure ret

-- | Submits a sqe with associated custom data. The data will be associated at
-- the end of @action
withSqeData :: (MonadIO m, MonadThrow m) => URing -> a -> (Sqe () -> m b) -> m b
withSqeData uring customData action = do
  sqe <- getSqe uring
  ret <- action sqe
  sqe' <- sqeSetData sqe customData
  pure ret

-- | given an IO computation, acquires a sqe for it, submits it, waits for it's
-- completion, and marks it as seen
submitAndComplete ::
  (MonadIO m, MonadThrow m) => URing -> (Sqe () -> m a) -> m a
submitAndComplete uring action = do
  sqe <- getSqe uring
  ret <- action sqe
  void $ submit uring
  withCqe $ \cqe -> do
    void $ waitCqe uring cqe
    cqeSeen uring cqe
  pure ret
