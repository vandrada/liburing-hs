module Cqe
  ( cqeTests,
  )
where

import Control.Concurrent.URing
import Test.Hspec

cqeTests :: Spec
cqeTests = describe "CQE" $ do
  testGetDataNotSet

testGetDataNotSet :: Spec
testGetDataNotSet = it "produces ()" $
  example $ do
    withURing $ \uring -> withURingQueue 1 uring [] $ \_ -> do
      sqe <- getSqe uring
      prepNop sqe
      submitted <- submit uring
      d <- withCqe $ \cqe -> do
        waitCqe uring cqe
        d <- cqeGetData cqe
        cqeSeen uring cqe
        pure d
      d `shouldBe` (Nothing :: Maybe ())
