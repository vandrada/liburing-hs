{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent.URing
  ( Completion (userData),
    RingM,
    complete,
    completeNr,
    setUserData,
    submitRing,
    uRead,
    withQueue,
    withRing,
  )
import Control.Monad (forM)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.IO.Unlift
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import Foreign
import Foreign.C
import System.Posix

-- how many bytes to read at once
bufSize :: Int
bufSize = 32

-- given a file descriptor, return the size of the underlying file
getFileSize :: Fd -> IO Int
getFileSize fd = fromIntegral . fileSize <$> getFdStatus fd

-- given a file descriptor, read the entirety of the backing file
readEntire :: Fd -> RingM BS.ByteString
readEntire fd = do
  size <- liftIO $ getFileSize fd
  res <- go fd size 0
  pure $ mconcat res
  where
    go :: Fd -> Int -> Int -> RingM [BS.ByteString]
    go fd size offset =
      if offset > size
        then -- we're done, submit and get all read results back
        do
          numSubmitted <- submitRing
          -- complete all submissions
          completeNr numSubmitted $ \comp ->
            case userData comp of
              Nothing -> pure ("" :: BS.ByteString)
              Just buf -> do
                liftIO $ BS.packCString buf
        else do
          withRunInIO $ \runInIO ->
            BS.useAsCStringLen (BS.replicate bufSize 0) $ \(buf, len) -> runInIO $ do
              -- create a submission to read
              submission <- uRead buf len fd offset
              -- set the user data to our CString
              submission `setUserData` buf
              go fd size (offset + bufSize)

completionsToString :: [Completion CString] -> IO BS.ByteString
completionsToString completions = do
  strings <- forM completions $ \completion -> do
    -- get back our CString
    case userData completion of
      Nothing -> pure ""
      Just buf -> do
        s <- BS.packCString buf
        free buf
        pure s
  pure $ mconcat strings

main :: IO ()
main = do
  s <- withRing $ \ring -> withQueue ring 512 $ \_ -> do
    fd <- liftIO $ openFd "bench/lorem.txt" ReadOnly Nothing defaultFileFlags
    -- prep submissions to read
    readEntire fd
  BS8.putStrLn s
