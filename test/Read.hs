module Read where

import Common (getResultData)
import Control.Concurrent.URing
import Control.Monad
  ( forM,
    forM_,
  )
import Control.Monad.IO.Class (liftIO)
import Control.Monad.IO.Unlift
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import Data.Maybe (catMaybes)
import Foreign
import Foreign.C
import System.Posix
import Test.Hspec

readTests :: Spec
readTests = describe "prepRead" $ do
  testReadVerbose
  testReadHigh
  testReadV
  testReadSingleChar
  testReadNr

getData :: Cqe -> IO (Int, Maybe String)
getData cqe = do
  cqeData <- cqeGetData cqe
  string <- traverse peekCString cqeData
  cqeResult <- cqeResult cqe
  pure (fromIntegral cqeResult, string)

testReadVerbose :: Spec
testReadVerbose = it "reads from fd" $
  example $ do
    withURing $ \uring -> withURingQueue 10 uring [] $ \_ -> do
      (fileName, fileContents) <- testFileAndContents
      fd <- openFd fileName ReadOnly Nothing defaultFileFlags
      sqe <- getSqe uring
      let buf = BS.replicate 20 0
      BS8.useAsCString buf $ \buf' -> do
        prepRead sqe fd buf' 20 0
        sqeSetData sqe buf'
        submitted <- submit uring
        withCqe $ \cqe -> do
          waitCqe uring cqe
          (res, readData) <- getData cqe
          res `shouldBe` length fileContents
          readData `shouldBe` Just fileContents
          cqeSeen uring cqe
      closeFd fd

testReadHigh :: Spec
testReadHigh = it "reads using higher level constructs" $
  example $ do
    (fileName, fileContents) <- testFileAndContents
    completions <- withRing $ \ring -> withQueue ring 512 $ \_ -> do
      fd <- liftIO $ openFd fileName ReadOnly Nothing defaultFileFlags
      (buf, len) <- liftIO . newCStringLen $ replicate (length fileContents) ' '
      sub <- uRead buf len fd 0
      setUserData sub buf
      uClose fd
      submitted <- submitRing
      completions <- complete submitted
      pure $ (\comp -> (completionResult comp, userData comp)) <$> completions

    length completions `shouldBe` 2
    let completion = head completions
    fst completion `shouldBe` fromIntegral (length fileContents)
    s <- traverse peekCString $ snd completion
    s `shouldBe` Just fileContents

-- reads a single byte at a time to demonstrate `runN`
testReadSingleChar :: Spec
testReadSingleChar = it "ringM one char" $
  example $ do
    (fileName, fileContents) <- liftIO testFileAndContents
    fd <- liftIO $ openFd fileName ReadOnly Nothing defaultFileFlags
    readData <- withRing $
      \uring -> withQueue uring 10 $ \_ -> mkRings fd (length fileContents) 0 []
    readData `shouldBe` fileContents
    liftIO $ closeFd fd
  where
    mkRings :: Fd -> Int -> Int -> [RingSubmission (Ptr CChar)] -> RingM String
    mkRings _ 0 _ acc = do
      submitted <- submitRing
      completed <- complete submitted
      liftIO $ flatten completed
    mkRings fd bytes offset acc = withRunInIO $ \runInIO -> do
      BS.useAsCStringLen (BS.replicate 1 0) $ \(buf', len) -> runInIO $ do
        sqe <- uRead buf' len fd offset
        sqe' <- setUserData sqe buf'
        mkRings fd (bytes - 1) (offset + 1) (sqe' : acc)

    flatten :: [Completion (Ptr CChar)] -> IO String
    flatten results = foldMap peekCString $ catMaybes (userData <$> results)

testReadV :: Spec
testReadV = it "reads using iovec" $
  example $ do
    (fileName, fileContents) <- liftIO testFileAndContents
    fd <- liftIO $ openFd fileName ReadOnly Nothing defaultFileFlags
    allocaBytes 9 $ \buf -> do
      completions <- withURing $ \uring -> withQueue uring 10 $ \_ -> do
        withRunInIO $ \runInIO ->
          withArrayLen (mkVecs buf 9 0 mempty) $ \len vecs -> runInIO $ do
            uReadV fd vecs len 0
            submitted <- submitRing
            complete submitted
      fmap completionResult completions `shouldBe` pure 9
      s <- peekCString (castPtr buf)
      s `shouldBe` take 9 fileContents
  where
    mkVecs buf nBytes offset acc
      | nBytes <= 0 =
        reverse acc
      | otherwise =
        let ioVec = IOVec (buf `plusPtr` offset) 3
         in mkVecs buf (nBytes - 3) (offset + 3) (ioVec : acc)

testReadNr :: Spec
testReadNr = it "waits for multiple" $
  example $ do
    (fileName, fileContents) <- liftIO testFileAndContents
    fd <- liftIO $ openFd fileName ReadOnly Nothing defaultFileFlags
    allocaBytes 9 $ \buf -> do
      withURing $ \uring -> withQueue uring 3 $ \_ -> do
        uRead buf 2 fd 0
        uRead (buf `plusPtr` 2) 2 fd 2
        uRead (buf `plusPtr` 4) 2 fd 4
        submitted <- submitRing
        completeNr submitted

fileName :: String
fileName = "test/read.txt"

testFileAndContents :: IO (String, String)
testFileAndContents = do
  contents <- readFile fileName
  pure (fileName, contents)
