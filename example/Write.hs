{-# LANGUAGE OverloadedStrings #-}

module Write
  ( testWrite,
    testLinkedWrite,
  )
where

import Control.Concurrent.URing
import Control.Monad
  ( forM,
    forM_,
    replicateM_,
    void,
  )
import qualified Data.ByteString.Char8 as BS8
import System.Posix.IO

type Meta = (Int, BS8.ByteString)

newtype Submission = Submission {submissionId :: Int} deriving (Show)

toBuf :: Submission -> BS8.ByteString
toBuf = BS8.pack . show . submissionId

testLinkedWrite :: URing -> IO ()
testLinkedWrite uring = do
  let count = 10
  fd <- openFd "example/test_out_sync.txt" WriteOnly (Just 644) defaultFileFlags

  sqeWithSubmission <- forM [1 .. count] $ \n -> do
    sqe <- getSqe uring
    pure (sqe, Submission n)

  -- starts a chain of asynchronous, but chained writes: writing "2" will only
  -- be done after writing "1" has completed and writing "3" only after writing
  -- "2", etc.
  forM_ sqeWithSubmission $ \(sqe, submission) -> do
    let toWrite = toBuf submission <> "\n"
    BS8.useAsCStringLen toWrite $ \(buf', len) ->
      prepWrite sqe fd buf' len (10 * submissionId submission)
    sqeSetData sqe submission
    sqeSetFlags sqe [Link]

  ready <- sqReady uring
  putStrLn $ "have: " <> show ready <> " submissions ready"

  -- tail of the link since it has no `Link` flag: sync the file
  uring `withSqe` \sqe -> do
    prepFsync sqe fd []
    sqeSetData sqe (Submission 0)

  void $ submit uring

  ready' <- cqReady uring
  putStrLn $ "have: " <> show ready' <> " completions ready"

  replicateM_ (count + 1) $ do
    completed <- withCqeData $ cqeWaitAndSee uring
    print (completed :: Maybe Submission)

testWrite :: URing -> IO ()
testWrite uring = do
  let count = 10
  fd <- openFd "example/test_out.txt" WriteOnly (Just 644) defaultFileFlags

  -- create `count` jobs to write to fd and associate a custom data type with
  -- the submission
  forM_ [1 .. count] $ \n ->
    BS8.useAsCStringLen (BS8.pack ("job: " <> show n <> "\n")) $
      \(buf', len) -> do
        submitted <- BS8.packCStringLen (buf', len)
        uring `withSqeData` (n, submitted) $ \sqe ->
          prepWrite sqe fd buf' len (n * len)

        -- submit all jobs at once
        void $ submit uring

  -- complete `count` jobs and get the associated data from it
  forM_ [1 .. count] $ \_ -> do
    completed <- withCqe $ \cqe -> do
      waitCqe uring cqe
      res <- cqeResult cqe
      print res
      d <- cqeGetData cqe
      cqeSeen uring cqe
      pure d
    putStrLn $ "completed " <> show (completed :: Maybe Meta)

  uring `submitAndComplete` \sqe -> do
    putStrLn "syncing"
    prepFsync sqe fd [DataSync]

  closeFd fd
