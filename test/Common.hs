module Common
  ( getResultData,
  )
where

import Control.Concurrent.URing
import Foreign.C.String (peekCString)
import Foreign.C.Types (CChar)
import Foreign.Ptr (Ptr)

getResultData :: Completion (Ptr CChar) -> IO (Int, Maybe String)
getResultData result = do
  string <- traverse peekCString $ userData result
  pure (fromIntegral $ completionResult result, string)
