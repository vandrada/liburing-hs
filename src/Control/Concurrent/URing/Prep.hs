module Control.Concurrent.URing.Prep
  ( prepRead,
    prepReadV,
    prepWrite,
    prepFsync,
    prepNop,
    prepSend,
    prepRecv,
    prepClose,
    prepFallocate,
    FsyncFlag (DataSync),
    FallocMode
      ( KeepSize,
        Unshare,
        PunchHole,
        CollapseRange,
        ZeroRange,
        InsertRange
      ),
  )
where

import qualified Control.Concurrent.URing.FFI.IOVec as IOVec
import qualified Control.Concurrent.URing.FFI.Internal as FFI
import Control.Concurrent.URing.General
  ( collapseFlags,
  )
import Control.Concurrent.URing.Sqe (Sqe (ptr))
import Control.Monad.IO.Class
  ( MonadIO,
    liftIO,
  )
import Foreign.C.Types (CInt)
import Foreign.Ptr (Ptr)
import System.Posix.Types (Fd)

data FsyncFlag = DataSync

prepNop :: (MonadIO m) => Sqe b -> m ()
prepNop = liftIO . FFI.io_uring_prep_nop . ptr

-- | prepares an async read
prepRead :: (MonadIO m) => Sqe b -> Fd -> Ptr a -> Int -> Int -> m ()
{-# INLINE prepRead #-}
prepRead sqe fd buf nbytes offset =
  liftIO $
    FFI.io_uring_prep_read
      (ptr sqe)
      (fromIntegral fd)
      buf
      (fromIntegral nbytes)
      (fromIntegral offset)

prepReadV :: (MonadIO m) => Sqe a -> Fd -> Ptr IOVec.IOVec -> Int -> Int -> m ()
{-# INLINE prepReadV #-}
prepReadV sqe fd ioVecs numVecs offset =
  liftIO $
    FFI.io_uring_prep_readv
      (ptr sqe)
      (fromIntegral fd)
      ioVecs
      (fromIntegral numVecs)
      (fromIntegral offset)

-- | prepares an async write
prepWrite :: (MonadIO m) => Sqe b -> Fd -> Ptr a -> Int -> Int -> m ()
{-# INLINE prepWrite #-}
prepWrite sqe fd buf nbytes offset =
  liftIO $
    FFI.io_uring_prep_write
      (ptr sqe)
      (fromIntegral fd)
      buf
      (fromIntegral nbytes)
      (fromIntegral offset)

-- | prepares an async fsync
prepFsync :: (MonadIO m) => Sqe b -> Fd -> [FsyncFlag] -> m ()
{-# INLINE prepFsync #-}
prepFsync sqe fd flags =
  liftIO $
    FFI.io_uring_prep_fsync
      (ptr sqe)
      (fromIntegral fd)
      (collapseFlags fsyncFlagsToInt flags)
  where
    fsyncFlagsToInt DataSync = FFI.ioRingFsyncDataSync

prepSend :: (MonadIO m) => Sqe b -> Fd -> Ptr a -> Int -> Int -> m ()
{-# INLINE prepSend #-}
prepSend sqe sockfd buf len flags =
  liftIO $
    FFI.io_uring_prep_send
      (ptr sqe)
      (fromIntegral sockfd)
      buf
      (fromIntegral len)
      (fromIntegral flags)

prepRecv :: (MonadIO m) => Sqe b -> Fd -> Ptr a -> Int -> Int -> m ()
{-# INLINE prepRecv #-}
prepRecv sqe sockfd buf len flags =
  liftIO $
    FFI.io_uring_prep_recv
      (ptr sqe)
      (fromIntegral sockfd)
      buf
      (fromIntegral len)
      (fromIntegral flags)

prepClose :: (MonadIO m) => Sqe b -> Fd -> m ()
{-# INLINE prepClose #-}
prepClose sqe fd = liftIO $ FFI.io_uring_prep_close (ptr sqe) (fromIntegral fd)

data FallocMode
  = KeepSize
  | Unshare
  | PunchHole
  | CollapseRange
  | ZeroRange
  | InsertRange
  deriving (Show)

prepFallocate :: MonadIO m => Sqe b -> Fd -> [FallocMode] -> Int -> Int -> m ()
{-# INLINE prepFallocate #-}
prepFallocate sqe fd mode offset len =
  liftIO $
    FFI.io_uring_prep_fallocate
      (ptr sqe)
      (fromIntegral fd)
      (collapseFlags fallocToFlag mode)
      (fromIntegral offset)
      (fromIntegral len)
  where
    fallocToFlag :: FallocMode -> CInt
    fallocToFlag KeepSize = FFI.flKeepSize
    fallocToFlag Unshare = FFI.flUnshare
    fallocToFlag PunchHole = FFI.flPunchHole
    fallocToFlag CollapseRange = FFI.flCollapseRange
    fallocToFlag ZeroRange = FFI.flZeroRange
    fallocToFlag InsertRange = FFI.flInsertRange
