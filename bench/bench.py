#!/usr/bin/env python3

import statistics
import numpy as np
import argparse
import subprocess
import collections
import json
import matplotlib.pyplot as plt

BenchOutput = collections.namedtuple("BenchOutput", ["regular", "uring",
                                                     "speedup"])

def parse_args():
    parser = argparse.ArgumentParser(description='bench liburing')
    parser.add_argument("benchtype", metavar='TYPE', choices=['read', 'write'])
    parser.add_argument('--times', dest='times', action='store', default=100)
    return parser.parse_args()

def _get_local_root():
    stack = subprocess.run(["stack", "path", "--local-install-root"], capture_output=True)
    return stack.stdout.split()[0].decode('utf-8')

def _bench_output_from_str(output):
    parts = [s.decode('utf-8') for s in output.split()]
    return BenchOutput(*parts)

def get_bin_path():
    root = _get_local_root()
    return f"{root}/bin/bench"


def build_package():
    subprocess.run(["stack", "build"], capture_output=True)


def run_benchmark(path, benchtype):
    result = subprocess.run([path, benchtype], capture_output=True)
    return _bench_output_from_str(result.stdout)


def analyze(runs):
    def _foo(times):
        return {
            "min": min(times),
            "max": max(times),
            "mean": statistics.mean(times),
            "median": statistics.median(times),
        }

    speedups = [float(run.speedup) for run in runs]
    urings = [float(run.uring) for run in runs]
    regulars = [float(run.regular) for run in runs]
    return {
        "speedup": _foo(speedups),
        "uring": _foo(urings),
        "regulars": _foo(regulars),
    }

def make_plot(results):
    regular = [float(result.regular) for result in results]
    uring = [float(result.uring) for result in results]
    x = np.arange(len(results))
    width = 0.35
    fig, ax = plt.subplots()
    regular_bar = ax.bar(x - width / 2, regular, width, label='sequential')
    uring_bar = ax.bar(x + width / 2, uring, width, label='uring')
    ax.set_ylabel('Runtime')
    ax.set_xticks(x)
    ax.legend()
    #ax.bar_label(regular_bar, padding=3)
    #ax.bar_label(uring_bar, padding=3)
    fig.tight_layout()
    plt.show()


def main():
    args = parse_args()
    build_package()
    binary = get_bin_path()
    results = [run_benchmark(binary, args.benchtype) for i in range(int(args.times))]
    analysis = analyze(results)
    make_plot(results)

    print(json.dumps(analysis))

if __name__ == '__main__':
    main()
