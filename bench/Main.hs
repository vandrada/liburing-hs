{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Concurrent.URing
import Control.Exception
  ( SomeException,
    catch,
    try,
  )
import Control.Monad
  ( forM,
    forM_,
    replicateM,
    replicateM_,
    void,
    when,
  )
import Control.Monad.IO.Class (liftIO)
import Control.Monad.IO.Unlift
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import Data.Char (ord)
import Data.Word (Word8)
import Foreign (Ptr, allocaBytes, new, plusPtr)
import Foreign.C
import Options.Applicative.Simple
  ( addCommand,
    simpleOptions,
  )
import System.CPUTime (getCPUTime)
import System.Exit (exitFailure)
import System.Posix
import Text.Printf (printf)

times :: Int
times = 500

toRead :: String
toRead = "bench/random.txt"

data Bench = ReadBench | WriteBench

bench :: IO a -> IO (a, Double)
bench action = do
  start <- getCPUTime
  v <- action
  end <- getCPUTime
  let diff = fromIntegral (end - start) / (10 ^ 12)
  return (v, diff)

rawWrite :: [BS.ByteString] -> IO ()
rawWrite bufs = do
  fd <- openFd "bench/out.txt" ReadWrite (Just 0o777) defaultFileFlags
  write fd bufs 0
  closeFd fd
  where
    write fd (b : bs) offset = do
      let len = BS.length b
      buf' <- new . BS.head $ b
      fdWriteBuf fd buf' (fromIntegral len)
      write fd bs (len + offset)
    write _ [] _ = pure ()

uringWrite :: [BS.ByteString] -> IO ()
uringWrite bufs = do
  withRing $ \ring -> withQueue ring 512 $ \_ -> do
    fd <-
      liftIO $
        openFd "bench/out.txt" ReadWrite (Just 0o777) defaultFileFlags
    write fd bufs 0
    uClose fd
    submitted <- submitRing
    completeNr_ submitted
    pure ()
  where
    write :: Fd -> [BS.ByteString] -> Int -> RingM ()
    write fd (b : bs) offset = do
      uWrite b fd offset
      write fd bs (offset + BS.length b)
    write _ [] _ = pure ()

bufSize :: Int
bufSize = 32

rawRead :: IO String
rawRead = do
  fd <- openFd toRead ReadOnly Nothing defaultFileFlags
  s <- read fd
  closeFd fd
  pure s
  where
    read :: Fd -> IO String
    read fd = concat . reverse <$> go fd mempty

    go :: Fd -> [String] -> IO [String]
    go fd acc = do
      (s, count) <-
        fdRead fd (fromIntegral bufSize)
          `catch` \(e :: SomeException) -> pure ("", 0)
      if count <= 0 then pure acc else go fd (s : acc)

uringRead :: IO String
uringRead = do
  -- get the file size
  stat <- liftIO $ getFileStatus toRead
  withRing $ \ring -> withQueue ring 512 $ \_ -> do
    fd <- liftIO $ openFd toRead ReadOnly Nothing defaultFileFlags
    s <- read fd (fromIntegral . fileSize $ stat)
    liftIO $ closeFd fd
    pure $ BS8.unpack s
  where
    read :: Fd -> Int -> RingM BS.ByteString
    read fd size = do
      withRunInIO $ \runInIO -> allocaBytes size $ \buf -> runInIO $ do
        go fd size buf 0 0
        liftIO $ BS.packCString buf

    go :: Fd -> Int -> Ptr a -> Int -> Int -> RingM Int
    go fd size buf offset acc =
      if offset > size
        then do
          submitted <- submitRing
          completeNr_ submitted
          pure acc
        else do
          sub <- uRead (buf `plusPtr` offset) bufSize fd offset
          full <- queueFull
          completed <-
            if full
              then do
                submitted <- submitRing
                completeNr_ submitted
                pure submitted
              else pure 0
          go fd size buf (offset + bufSize) (completed + acc)

main :: IO ()
main = do
  (_, benchType) <- parseArgs
  (regularTime, uringTime) <- case benchType of
    ReadBench -> do
      (res, uringTime) <- bench uringRead
      (res', regularTime) <- bench rawRead
      when (res /= res') $ do
        print "failed"
        exitFailure
      pure (regularTime, uringTime)
    WriteBench -> do
      let bufs =
            BS.pack . pure . fromIntegral . ord
              <$> take
                times
                (cycle ['a' .. 'z'])
      (result, uringTime) <- bench $ uringWrite bufs
      (_, regularTime) <- bench $ rawWrite bufs
      pure (regularTime, uringTime)
  let speedup = regularTime / uringTime
  printf "%g\t %g\t %g\n" regularTime uringTime speedup

parseArgs :: IO ((), Bench)
parseArgs = simpleOptions "ver" "header" "desc" (pure ()) $ do
  addCommand "read" "run read benchmark" (const ReadBench) (pure ())
  addCommand "write" "run write benchmark" (const WriteBench) (pure ())
