{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent.URing
import Read
import System.IO
import Write

main :: IO ()
main = withURing $ \uring ->
  withFile "example/test.txt" ReadWriteMode $ \handle -> do
    hPutStr handle "hello world"
    testReadHigh uring
    testReadSingleChar uring
    testRead uring
    testReadVerbose uring

--testWrite uring
--  testLinkedWrite uring
