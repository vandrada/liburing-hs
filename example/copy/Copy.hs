module Main where

import Control.Concurrent.URing
import Control.Monad.IO.Class (liftIO)
import Foreign
import System.Environment
import System.Posix

data Operation = Read | Write

data IOData = IOData
  { operation :: Operation,
    firstOffset :: Int,
    offset :: Int,
    ioVec :: Ptr IOVec
  }

queueRead :: Fd -> Int -> Int -> RingM (RingSubmission IOData)
queueRead fd size offset = do
  buf <- liftIO $ mallocBytes size
  ioVecs <- liftIO $ newArray [IOVec buf (fromIntegral size)]
  let d = IOData Read offset offset ioVecs
  sqe <- uReadV fd ioVecs 1 offset
  withUserData d sqe

queueWrite :: RingM ()
queueWrite = pure ()

getFdSize :: Fd -> IO Int
getFdSize fd = fromIntegral . fileSize <$> getFdStatus fd

copyFile :: Int -> RingM ()
copyFile inSize = pure ()

main :: IO ()
main = do
  (inFile : outFile : _) <- getArgs
  inFd <- openFd inFile ReadOnly Nothing defaultFileFlags
  outFd <- openFd outFile WriteOnly (Just 0o644) defaultFileFlags {trunc = True}
  inSize <- getFdSize inFd
  withRing $ \ring -> withQueue ring 512 $ \_ -> do
    copyFile inSize
    pure ()
  closeFd inFd
  closeFd outFd
  pure ()
