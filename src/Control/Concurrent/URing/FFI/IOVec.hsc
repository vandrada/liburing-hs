{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE CPP #-}

module Control.Concurrent.URing.FFI.IOVec (IOVec(..)) where

import Foreign.C.Types (CSize)
import Foreign.Ptr (Ptr)
import Foreign.Storable (Storable(..))
import Data.Word (Word8)

#include <sys/uio.h>

data IOVec = IOVec
    { iov_base :: !(Ptr Word8)
    , iov_len :: !CSize
    } deriving (Show)

instance Storable IOVec where
  sizeOf _ = #{size struct iovec}
  alignment _ = #{alignment struct iovec}
  peek p = IOVec <$> (#{peek struct iovec, iov_base}) p <*> (#{peek struct iovec, iov_len}) p
  poke ptr iovec = do
      #{poke struct iovec, iov_base} ptr (iov_base iovec)
      #{poke struct iovec, iov_len} ptr (iov_len iovec)

