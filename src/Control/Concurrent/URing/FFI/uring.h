/* taken from /usr/include/liburing.h */
#ifndef _URING_H
#define _URING_H

#include <liburing.h>
#include <sys/uio.h>

int opcode_supported(struct io_uring_probe *p, int op);
void cq_advance(struct io_uring *ring, unsigned nr);
void cqe_seen(struct io_uring *ring, struct io_uring_cqe *cqe);
void sqe_set_data(struct io_uring_sqe *sqe, void *data);
void *cqe_get_data(const struct io_uring_cqe *cqe);
void sqe_set_flags(struct io_uring_sqe *sqe, unsigned flags);
void prep_rw(int op, struct io_uring_sqe *sqe, int fd,
				    const void *addr, unsigned len,
				    __u64 offset);
void prep_readv(struct io_uring_sqe *sqe, int fd,
				       const struct iovec *iovecs,
				       unsigned nr_vecs, off_t offset);
void prep_read_fixed(struct io_uring_sqe *sqe, int fd,
					    void *buf, unsigned nbytes,
					    off_t offset, int buf_index);
void prep_writev(struct io_uring_sqe *sqe, int fd,
					const struct iovec *iovecs,
					unsigned nr_vecs, off_t offset);
void prep_write_fixed(struct io_uring_sqe *sqe, int fd,
					     const void *buf, unsigned nbytes,
					     off_t offset, int buf_index);
void prep_recvmsg(struct io_uring_sqe *sqe, int fd,
					 struct msghdr *msg, unsigned flags);
void prep_sendmsg(struct io_uring_sqe *sqe, int fd,
					 const struct msghdr *msg, unsigned flags);
void prep_poll_add(struct io_uring_sqe *sqe, int fd,
					  short poll_mask);
void prep_poll_remove(struct io_uring_sqe *sqe,
					     void *user_data);
void prep_fsync(struct io_uring_sqe *sqe, int fd,
				       unsigned fsync_flags);

void prep_nop(struct io_uring_sqe *sqe);
void prep_timeout(struct io_uring_sqe *sqe,
					 struct __kernel_timespec *ts,
					 unsigned count, unsigned flags);
void prep_timeout_remove(struct io_uring_sqe *sqe,
						__u64 user_data, unsigned flags);
void prep_accept(struct io_uring_sqe *sqe, int fd,
					struct sockaddr *addr,
					socklen_t *addrlen, int flags);
void prep_cancel(struct io_uring_sqe *sqe, void *user_data, int flags);
void prep_link_timeout(struct io_uring_sqe *sqe,
					      struct __kernel_timespec *ts,
					      unsigned flags);
void prep_connect(struct io_uring_sqe *sqe, int fd,
					 struct sockaddr *addr,
					 socklen_t addrlen);
void prep_files_update(struct io_uring_sqe *sqe,
					      int *fds, unsigned nr_fds,
					      int offset);
void prep_fallocate(struct io_uring_sqe *sqe, int fd,
					   int mode, off_t offset, off_t len);
void prep_openat(struct io_uring_sqe *sqe, int dfd,
					const char *path, int flags, mode_t mode);
void prep_close(struct io_uring_sqe *sqe, int fd);
void prep_read(struct io_uring_sqe *sqe, int fd,
				      void *buf, unsigned nbytes, off_t offset);
void prep_write(struct io_uring_sqe *sqe, int fd,
				       const void *buf, unsigned nbytes, off_t offset);
struct statx;
void prep_statx(struct io_uring_sqe *sqe, int dfd,
				const char *path, int flags, unsigned mask,
				struct statx *statxbuf);
void prep_fadvise(struct io_uring_sqe *sqe, int fd,
					 off_t offset, off_t len, int advice);
void prep_madvise(struct io_uring_sqe *sqe, void *addr,
					 off_t length, int advice);
void prep_send(struct io_uring_sqe *sqe, int sockfd,
				      const void *buf, size_t len, int flags);
void prep_recv(struct io_uring_sqe *sqe, int sockfd,
				      void *buf, size_t len, int flags);
void prep_openat2(struct io_uring_sqe *sqe, int dfd,
					const char *path, struct open_how *how);
struct epoll_event;
void prep_epoll_ctl(struct io_uring_sqe *sqe, int epfd,
   				   int fd, int op,
   				   struct epoll_event *ev);
void prep_provide_buffers(struct io_uring_sqe *sqe,
   					 void *addr, int len, int nr,
   					 int bgid, int bid);
void prep_remove_buffers(struct io_uring_sqe *sqe, int nr, int bgid);
unsigned sq_ready(struct io_uring *ring);
unsigned sq_space_left(struct io_uring *ring);
unsigned cq_ready(struct io_uring *ring);
int wait_cqe_nr(struct io_uring *ring,
				      struct io_uring_cqe *cqe_ptr,
				      unsigned wait_nr);
int peek_cqe(struct io_uring *ring,
				    struct io_uring_cqe **cqe_ptr);
int wait_cqe(struct io_uring *ring,
				    struct io_uring_cqe **cqe_ptr);

struct io_uring_cqe *foo(struct io_uring *ring, struct io_uring_cqe *, unsigned wait_nr);
#endif
