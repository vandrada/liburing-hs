module Control.Concurrent.URing.General
  ( collapseFlags,
    mkErrno,
    throwUnless,
  )
where

import Control.Exception (Exception)
import Control.Monad (unless)
import Control.Monad.Catch
  ( MonadThrow,
    throwM,
  )
import Control.Monad.IO.Class (MonadIO)
import Data.Bits
  ( Bits,
    (.|.),
  )
import Foreign.C.Types (CInt)

-- Used to do the usual `FLAG1 | FLAG2 | FLAG3` routine. Assumes 0 is the value
-- for no flags. `convert` should be a way to go from Haskell-types to C-types
collapseFlags :: (Num b, Bits b) => (a -> b) -> [a] -> b
collapseFlags convert flags = foldl (.|.) 0 $ map convert flags

throwUnless :: (Exception e, MonadThrow m, MonadIO m) => Bool -> e -> m ()
throwUnless cond exception = unless cond $ throwM exception

mkErrno :: CInt -> Int
mkErrno = fromIntegral . (* (-1))
