module Drain where

import Control.Concurrent.URing
import Test.Hspec

drainTests :: Spec
drainTests = testDrain

testDrain :: Spec
testDrain = it "drains" $
  example $ do
    withURing $ \uring -> withURingQueue 10 uring [] $ \queue -> do
      pure ()
