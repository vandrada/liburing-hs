# liburing-hs

Haskell FFI binding for [liburing](https://git.kernel.dk/cgit/liburing/).

More info on `liburing` can be found [here](https://kernel.dk/io_uring.pdf) and
[here](https://unixism.net/loti/async_intro.html)

## Overview
There are two layers to this library:
* raw, low-level bindings (`Control.Concurrent.URing.FFI`)
* somewhat higher level Haskell specific bindings (`Control.Concurrent.URing`)

The low-level bindings try to do the bare minimum, they just wrap `liburing`
itself with no ceremony about it. The higher-level ones try to apply some
Haskell idioms on top of the low-level ones.

Since liburing has some inline functions defined, the file `uring.[c|h]` is used
so they can be bound to. I'm sure there's a better way to do this, but it works
for now.

Look in `examples` for examples.

Tested with liburing version 0.6-1 and kernel 5.6.14

## Limitations
### Inline Functions
`liburing` has numerous inline functions defined, since these don't exist in the
library, they can't be bound to. `uring.c` exists to make them regular C
functions at the cost of not being able to inline them.

## Completed So Far
* `io_uring_prep_read`
* `io_uring_prep_write`
* `io_uring_prep_recv`
* `io_uring_prep_send`
* `io_uring_prep_fsync`
* `io_uring_prep_fallocate`
* `io_uring_prep_close`
* `sqe_set_data`
* `cqe_get_data`
* `sqe_set_flags`
* `sq_space_left`
* `sq_ready`
* `cq_ready`
* `peek_cqe`

## TODO

* [ ] **Error handling**
* [ ] Add flags for the various calls
* [ ] Add proper tests
* [ ] Clean up package.yaml/cabal

### Functions
* cq_advance
* io_uring_prep_splice
* io_uring_prep_readv
* io_uring_prep_read_fixed
* io_uring_prep_writev
* io_uring_prep_write_fixed
* io_uring_prep_recvmsg
* io_uring_prep_sendmsg
* io_uring_prep_poll_add
* io_uring_prep_poll_remove
* io_uring_prep_timeout
* io_uring_prep_timeout_remove
* io_uring_prep_accept
* io_uring_prep_cancel
* io_uring_prep_link_timeout
* io_uring_prep_connect
* io_uring_prep_files_update
* io_uring_prep_openat
* io_uring_prep_statx
* io_uring_prep_fadvise
* io_uring_prep_madvise
* io_uring_prep_openat2
* io_uring_prep_epoll_ctl
* io_uring_prep_provide_buffers
* io_uring_prep_remove_buffers
* wait_cqe_nr
